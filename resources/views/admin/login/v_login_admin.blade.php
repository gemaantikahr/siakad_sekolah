<!DOCTYPE html>
<html>
    @include('admin/partials/header')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
      <b>Admin</b>Login</a>
  </div>

  @if(\Session::has('alert'))
  <div class="alert alert-danger">
      <div>{{Session::get('alert')}}</div>
  </div>
@endif
@if(\Session::has('alert-success'))
  <div class="alert alert-success">
      <div>{{Session::get('alert-success')}}</div>
  </div>
@endif
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form method="POST" action="/admin/login/login_cek">
        {{ csrf_field() }}
        <div class="input-group mb-3">
          <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" id="xusername" onkeypress="return AvoidSpace(event)" name="username" value="{{ old('email') }}" required autocomplete="email" autofocus>
          @error('email')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="/admin/login/register" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
<script>
    function AvoidSpace(event) {
        var k = event ? event.which : window.event.keyCode;
        if (k == 32) return false;
    }
    </script>
    <script type="text/javascript">
      function Validate() {
          var password = document.getElementById("txtPassword").value;
          var confirmPassword = document.getElementById("txtConfirmPassword").value;
          if (password != confirmPassword) {
              alert("Passwords do not match.");
              return false;
          }
          return true;
      }
    </script>

@include('admin/partials/js')
</body>
</html>
