@extends('admin/admin')

@section('judulhalaman', 'Kelas')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Kelas</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahdata">
                        Tambah Data
                    </button>
                    <hr>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kelas</th>
                  <th>Extensi</th>
                  <th>Banyak Siswa</th>
                  <th>Nama Guru</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0?>
                @foreach ($kelas as $data)
                <?php $no++?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{ "Kelas ".$data->kelas_kelas}}</td>
                  <td>{{ $data->kelas_nama}}</td>
                  <td>
                        <?php
                        if (in_array($data->id, array_keys($banyak_siswa))) {
                            echo $banyak_siswa[$data->id];
                        }else {
                            echo "Belum Ada Siswa";
                        }
                        ?>
                  </td>
                  <td>{{ $data->guru_nama}}</td>
                  <td width="20%">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modaledit{{$data->id}}">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                        <i class="fas fa-trash"></i>
                    </button>
                    <a href="/admin/kelas/detail_kelas/{{ $data->id }}" type="button" class="btn btn-primary">
                        <i class="fas fa-info-circle"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

    {{-- modal tambah --}}
    <div class="modal fade" id="tambahdata">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Kelas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <div class="card-body">
          </button>
        </div>
        <div class="modal-body">
            <form role="form" action="/admin/kelas/tambah_kelas" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kelas</label>
                    <input type="number" class="form-control" name="xkelas" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Extensi Kelas</label>
                    <input type="text" class="form-control" placeholder="*contoh : A" name="xextensi" required>
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                        <label>Guru</label>
                        <select class="form-control" name="xguru">
                            @foreach ($guru as $data)
                                <option value="{{ $data->id }}">{{ $data->guru_nama }}</option>
                            @endforeach
                        </select>
                      </div>
                  </div>
                <!-- /.card-body -->
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>

      </div>
    </div>
    </div>
    {{-- modal tambah --}}

    {{-- modal edit --}}
    @foreach ($kelas as $item)
        <div class="modal fade" id="modaledit{{ $item->id }}">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form role="form" action="/admin/kelas/edit_kelas" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group" hidden>
                                <label for="exampleInputEmail1">Id</label>
                                <input type="text" class="form-control" name="xid" value="{{ $item->id }}" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" class="form-control" name="xkelas" value="{{ $item->kelas_kelas }}" required readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Extensi</label>
                                <input type="text" class="form-control" name="xextensi" value="{{ $item->kelas_nama }}" required>
                            </div>

                            <div class="form-group">
                            <div class="form-group">
                                <label>Guru</label>
                                <select class="form-control" name="xguru">
                                    <option value="{{ $item->id_guru }}">{{ $data->guru_nama }}</option>
                                    @foreach ($guru as $data)
                                        <option value="{{ $data->id }}">{{ $data->guru_nama }}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                        <!-- /.card-body -->
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>

            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- modal edit --}}

    {{-- modal delete --}}
    @foreach ($kelas as $item)
        <div class="modal fade" id="modalhapus{{ $item->id }}">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p> Anda yakin ingin menghapus data <strong>{{ $item->kelas_kelas." ".$item->kelas_nama }}</strong> .?</p>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <a href="/admin/kelas/hapus_kelas/{{ $item->id }}" type="submit" class="btn btn-danger">Hapus</a>
                </div>


            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- modal delete --}}

@endsection
