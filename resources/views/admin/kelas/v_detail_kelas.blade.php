@extends('admin/admin')

@section('judulhalaman', 'Detail Kelas')

@section('content')
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
</head>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-primary">
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Kelas {{ $kelas_kelas." ".$kelas_nama }}</h3>
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Jumlah Siswa <span class="float-right badge bg-primary">{{ $jumlah_siswa }}</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Jumlah Siswa Laki-Laki <span class="float-right badge bg-primary">{{ $jumlah_siswa_l }}</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Jumlah Siswi Perempuan <span class="float-right badge bg-primary">{{ $jumlah_siswa_p }}</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
          {{-- data guru --}}
          @foreach ($guru as $data)
          <div class="col-md-8">
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                <h3 class="widget-user-username">Wali Kelas</h3>
                <h5 class="widget-user-desc">{{ $data->guru_nama }}</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src='{{ asset("assets/images/guru/$data->guru_photo")}}' alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                          <h5 class="description-header">No Handphone</h5>
                          <span class="description-text">{{ $data->guru_no_hp }}</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">Alamat</h5>
                      <span>{{ $data->guru_alamat }}</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">Tanggal Masuk</h5>
                      <span>{{ $data->guru_tgl_masuk }}</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
          </div>
          @endforeach
          {{-- data guru --}}
        </div>
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Kelas {{ $kelas_kelas." ".$kelas_nama }}</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                      <div>
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahdata">
                              Tambah Data
                          </button>
                          <hr>
                      </div>
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>No</th>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Option</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php $no=0?>
                      @foreach ($siswa as $data)
                      <?php $no++?>
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$data->siswa_nisn}}</td>
                        <td>{{ $data->siswa_nama}}</td>
                        <td>
                            <?php  if($data->siswa_jenis_kelamin == "p"){ ?>
                            Perempuan
                            <?php }else{?>
                            Laki-Laki
                            <?php }?>
                        </td>
                        <td width="20%">
                          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modaledit{{$data->id}}">
                              <i class="fas fa-edit"></i>
                          </button>
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                              <i class="fas fa-trash"></i>
                          </button>
                          <a href="/admin/kelas/detail_kelas/{{ $data->id }}" type="button" class="btn btn-primary">
                              <i class="fas fa-info-circle"></i>
                          </a>
                        </td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
{{-- modal Tambah --}}
<div class="modal fade" id="tambahdata">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Default Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form role="form" action="/admin/kelas/tambah_siswa_kelas" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama/Nisn Siswa</label>
                    <br>
                    <select class="cari form-control" style="width:100%; heigth:60px;" name="xsiswa"></select>
                    <input type="text" name="xidkelas" value="{{ $id_kelas }}" hidden>
                  </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{-- modal Tambah --}}

{{-- modal edit --}}
@foreach ($siswa as $item)
    <div class="modal fade" id="modaledit{{ $item->id }}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Default Modal</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form role="form" action="/admin/kelas/edit_kelas" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group" hidden>
                            <label for="exampleInputEmail1">Id</label>
                            <input type="text" class="form-control" name="xid" value="{{ $item->id }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="text" class="form-control" name="xkelas" value="{{ $item->siswa_nama }}" required readonly>
                        </div>
                        <div class="form-group">
                        <div class="form-group">
                            <label>Kelas</label>
                            <select class="form-control" name="xguru">
                                <option>Pindah Kelas</option>
                                @foreach ($kelas as $data)
                                    <option value="{{ $data->id }}">{{ "Kelas ".$data->kelas_kelas." ".$data->kelas_nama }}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                    <!-- /.card-body -->
                    </div>
            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-primary">Edit</button>
            </div>
        </form>

        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endforeach
{{-- modal edit --}}

{{-- modal hapus --}}
@foreach ($siswa as $item)
    <div class="modal fade" id="modalhapus{{ $item->id }}">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Default Modal</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <p>Hapus data <strong>{{ $item->siswa_nisn }}</strong></p>
            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            <a href="/admin/kelas/hapus_siswa_kelas/{{ $item->siswa_nisn }}" type="button" class="btn btn-primary">Edit</a>
            </div>

        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endforeach
{{-- modal hapus --}}



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
  $('.cari').select2({
    placeholder: 'Tulis Nama / NISN',
    ajax: {
      url: '/cari',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.siswa_nama,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });

</script>


@endsection
