@extends('admin/admin')

@section('judulhalaman', 'Guru')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Guru</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahdata">
                        Tambah Data
                    </button>
                    <hr>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>No Telepon</th>
                  <th>Tanggal Masuk</th>
                  <th>Foto</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0?>
                @foreach ($guru as $data)
                <?php $no++?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$data->guru_nama}}</td>
                  <td>{{$data->guru_alamat}}</td>
                  <td>{{$data->guru_no_hp}}</td>
                  <td>{{$data->guru_tgl_masuk}}</td>
                    <td><img src='{{ asset("assets/images/guru/$data->guru_photo")}}' class="css-class" alt="alt text" height="75px;"></td>
                  <td width="15%">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modaledit{{$data->id}}">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                        <i class="fas fa-trash"></i>
                    </button>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

    {{-- modal tambah --}}
    <div class="modal fade" id="tambahdata">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Default Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form role="form" action="/admin/guru/tambah_guru" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" name="xnama" required>
                  </div>
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" rows="3" name="xalamat" required></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nomer Telephon</label>
                    <input type="text" class="form-control" name="xnohp" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Masuk</label>
                    <input type="date" class="form-control" name="xtglmasuk" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Gambar</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="xgambar">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    {{-- modal tambah --}}

    {{-- modal edit --}}
    @foreach ($guru as $item)
        <div class="modal fade" id="modaledit{{ $item->id }}">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form role="form" action="/admin/guru/edit_guru" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                        <div class="form-group" hidden>
                            <label for="exampleInputEmail1">Id</label>
                            <input type="text" class="form-control" name="xid" value="{{ $item->id }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="text" class="form-control" name="xnama" value="{{ $item->guru_nama }}" required>
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" rows="3" name="xalamat" required>{{ $item->guru_alamat }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nomer Telephon</label>
                            <input type="text" class="form-control" name="xnohp" value="{{ $item->guru_no_hp }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tanggal Masuk</label>
                            <input type="date" class="form-control" name="xtglmasuk" value="{{ $item->guru_tgl_masuk }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Gambar</label>
                            <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" name="xgambar">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- /.card-body -->
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>

            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- modal edit --}}

    {{-- modal delete --}}
    @foreach ($guru as $item)
        <div class="modal fade" id="modalhapus{{ $item->id }}">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p> Anda yakin ingin menghapus data <strong>{{ $item->guru_nama }}</strong> .?</p>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <a href="/admin/guru/hapus_guru/{{ $item->id }}" type="submit" class="btn btn-danger">Hapus</a>
                </div>


            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- modal delete --}}

@endsection
