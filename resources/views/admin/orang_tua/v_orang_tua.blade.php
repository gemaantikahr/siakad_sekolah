@extends('admin/admin');

@section('judulhalaman', 'siswa')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div>
                    <a href="/admin/orang_tua/tambah_data" type="button" class="btn btn-primary">
                        Tambah Data
                    </a>
                    <hr>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Alamat</th>
                  <th>Pekerjaan</th>
                  <th>Agama</th>
                  <th>Jenis Kelamin</th>
                  <th>Tanggal Lahir</th>
                  <th>No Telepon</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0?>
                @foreach ($orang_tua as $data)
                <?php $no++?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$data->ortu_nama}}</td>
                  <td>{{$data->ortu_nik}}</td>
                  <td>{{$data->ortu_alamat}}</td>
                  <td>{{$data->ortu_pekerjaan}}</td>
                  <td>{{$data->ortu_agama}}</td>
                  <td>
                      <?php  if($data->ortu_jk == "p"){ ?>
                      Perempuan
                      <?php }else{?>
                      Laki-Laki
                      <?php }?>
                  </td>
                  <td>{{$data->ortu_tgl_lahir}}</td>
                  <td>{{$data->ortu_no_hp}}</td>
                  <td width="20%">
                    <a href="/admin/orang_tua/edit_ortu/{{ $data->id }}" type="button" class="btn btn-primary">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                      <i class="fas fa-info-circle"></i>
                  </button>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


    {{-- modal delete --}}
    @foreach ($orang_tua as $item)
        <div class="modal fade" id="modalhapus{{ $item->id }}">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p> Anda yakin ingin menghapus data <strong>{{ $item->ortu_nama }}</strong> .?</p>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <a href="/admin/orang_tua/hapus_ortu/{{ $item->id }}" type="submit" class="btn btn-danger">Hapus</a>
                </div>


            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- modal delete --}}


@endsection
