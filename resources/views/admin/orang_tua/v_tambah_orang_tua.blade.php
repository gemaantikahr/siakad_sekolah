@extends('admin/admin')

@section('judulhalaman','Data Orang Tua')

@section('content')
<style>
#addLiveOthers {
  display:none;
}
</style>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Isi Data Orang Tua</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form action="/admin/orang_tua/simpan_orang_tua" method="POST">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="..." name="xnama" required>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>NIK</label>
                      <input type="text" class="form-control" placeholder="..." name="xnik" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="..." name="xalamat" required></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Jenis Kelamin</label>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="xjk" checked value="l">
                          <label for="customRadio1" class="custom-control-label">Laki-Laki</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="xjk" value="p">
                          <label for="customRadio2" class="custom-control-label">Perempuan</label>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label>Nomer Telepon</label>
                        <input type="text" class="form-control" placeholder="..." name="xnohp" required>
                      </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" class="form-control" placeholder="..." name="xtgllahir" required>
                          </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                        <!-- select -->
                        <div class="form-group">
                          <label>Agama</label>
                          <select class="form-control" name="xagama">
                            <option value="islam">Islam</option>
                            <option value="protestan">Kristen Protestan</option>
                            <option value="katolik">Katolik</option>
                            <option value="hindu">Hindu</option>
                            <option value="buddha">Buddha</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="">Pekerjaan</label>
                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" onclick="showText(1)" name="xpekerjaan" value="PNS">PNS
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" onclick="showText(2)" name="xpekerjaan" value="Wiraswasta">Wiraswasta
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" id="showAddLiveOthers" name="xpekerjaan" onclick="showText(5)">Lainnya
                            </label>
                            <input type="text" class="form-control" placeholder="..." name="xpekerjaanlain" id="addLiveOthers">
                          </div>
                    </div>
                    {{-- <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" class="form-control" placeholder="..." name="xtgllahir" required>
                          </div>
                    </div> --}}
                  </div>
                  <div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                  </div>
                </div>
            </form>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

  <script>
function showText(num){
  if(num==5){ document.getElementById('addLiveOthers').style.display='block';}
  else {document.getElementById('addLiveOthers').style.display='none';}
  return;
}
  </script>

@endsection
