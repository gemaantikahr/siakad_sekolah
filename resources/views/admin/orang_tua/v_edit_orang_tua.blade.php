@extends('admin/admin')

@section('judulhalaman','Data Siswa')

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Isi Data Ortu</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @foreach ($orang_tua as $data)
                <form action="/admin/orang_tua/update_data_ortu" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                          <label>Nama</label>
                          <input type="text" class="form-control" placeholder="..." name="xnama" value="{{ $data->ortu_nama }}"required>
                          <input type="text" class="form-control" placeholder="..." name="xid" value="{{ $data->id }}" required hidden>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>NIK</label>
                          <input type="text" class="form-control" placeholder="..." name="xnik" value="{{ $data->ortu_nik }}"required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <!-- textarea -->
                        <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" rows="3" placeholder="..." name="xalamat" required>{{ $data->ortu_alamat }}</textarea>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Jenis Kelamin</label>
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="xjk" value="l">
                              <label for="customRadio1" class="custom-control-label">Laki-Laki</label>
                            </div>
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="xjk" value="p">
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                          <!-- textarea -->
                          <div class="form-group">
                            <label>Nomer Telepon</label>
                            <input type="text" class="form-control" placeholder="..." name="xnohp" value="{{ $data->ortu_no_hp }}"required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control" placeholder="..." value="{{ $data->ortu_tgl_lahir }}"name="xtgllahir" required>
                              </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                            <!-- select -->
                            <div class="form-group">
                              <label>Agama</label>
                              <select class="form-control" name="xagama">
                                <option value="islam">Islam</option>
                                <option value="protestan">Kristen Protestan</option>
                                <option value="katolik">Katolik</option>
                                <option value="hindu">Hindu</option>
                                <option value="buddha">Buddha</option>
                              </select>
                            </div>
                        </div>
                        {{-- untuk pekerjaan --}}
                        <?php if($data->ortu_pekerjaan == 'PNS'){?>
                            <style>
                                #addLiveOthers {
                                  display:none;
                                }
                            </style>
                            <div class="col-sm-6">
                                <label for="">Pekerjaan</label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" onclick="showText(1)" name="xpekerjaan" value="PNS" checked>PNS
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" onclick="showText(2)" name="xpekerjaan" value="Wiraswasta">Wiraswasta
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input" id="showAddLiveOthers" name="xpekerjaan" onclick="showText(5)">Lainnya
                                    </label>
                                    <input type="text" class="form-control" placeholder="..." name="xpekerjaanlain" id="addLiveOthers">
                                </div>
                            </div>
                        <?php }elseif($data->ortu_pekerjaan == 'Wiraswasta'){?>
                            <style>
                                #addLiveOthers {
                                  display:none;
                                }
                            </style>
                            <div class="col-sm-6">
                                <label for="">Pekerjaan</label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" onclick="showText(1)" name="xpekerjaan" value="PNS">PNS
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" onclick="showText(2)" name="xpekerjaan" value="Wiraswasta" checked>Wiraswasta
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" id="showAddLiveOthers" name="xpekerjaan" onclick="showText(5)">Lainnya
                                    </label>
                                    <input type="text" class="form-control" placeholder="..." name="xpekerjaanlain" id="addLiveOthers">
                                  </div>
                            </div>
                        <?php }else{?>
                            <div class="col-sm-6">
                                <label for="">Pekerjaan</label>
                                <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" onclick="showText(1)" name="xpekerjaan" value="PNS">PNS
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" onclick="showText(2)" name="xpekerjaan" value="Wiraswasta">Wiraswasta
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="radio" class="form-check-input" id="showAddLiveOthers" name="xpekerjaan" onclick="showText(5)" checked>Lainnya
                                    </label>
                                    <input type="text" class="form-control" placeholder="..." value="{{ $data->ortu_pekerjaan }}" name="xpekerjaanlain" id="addLiveOthers">
                                  </div>
                            </div>
                        <?php }?>

                        {{-- untuk pekerjaan --}}
                      </div>
                      <div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                      </div>
                    </div>
                </form>
                @endforeach
            </div>
          <!-- /.card -->
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <script>
    function showText(num){
      if(num==5){ document.getElementById('addLiveOthers').style.display='block';}
      else {document.getElementById('addLiveOthers').style.display='none';}
      return;
    }
      </script>

@endsection
