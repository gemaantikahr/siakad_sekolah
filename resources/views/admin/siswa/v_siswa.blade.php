@extends('admin/admin');

@section('judulhalaman', 'siswa')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div>
                    <a href="/admin/siswa/tambah_data" type="button" class="btn btn-primary">
                        Tambah Data
                    </a>
                    <hr>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>NISN</th>
                  <th>Alamat</th>
                  <th>Agama</th>
                  <th>Jenis Kelamin</th>
                  <th>Tanggal Masuk</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=0?>
                @foreach ($siswa as $data)
                <?php $no++?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$data->siswa_nama}}</td>
                  <td>{{$data->siswa_nisn}}</td>
                  <td>{{$data->siswa_alamat}}</td>
                  <td>{{$data->siswa_agama}}</td>
                  <td>
                      <?php  if($data->siswa_jenis_kelamin == "p"){ ?>
                      Perempuan
                      <?php }else{?>
                      Laki-Laki
                      <?php }?>
                  </td>
                  <td>{{$data->siswa_tgl_masuk}}</td>
                  <td width="20%">
                    <a href="/admin/siswa/edit_siswa/{{ $data->id }}" type="button" class="btn btn-primary">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalhapus{{$data->id}}">
                      <i class="fas fa-info-circle"></i>
                  </button>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


    {{-- modal delete --}}
    @foreach ($siswa as $item)
        <div class="modal fade" id="modalhapus{{ $item->id }}">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p> Anda yakin ingin menghapus data <strong>{{ $item->siswa_nama }}</strong> .?</p>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <a href="/admin/siswa/hapus_siswa/{{ $item->id }}" type="submit" class="btn btn-danger">Hapus</a>
                </div>


            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    {{-- modal delete --}}


@endsection
