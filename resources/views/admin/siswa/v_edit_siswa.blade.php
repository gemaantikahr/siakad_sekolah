@extends('admin/admin')

@section('judulhalaman','Data Siswa')

@section('content')

<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- right column -->
        <div class="col-md-12">
          <!-- general form elements disabled -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Isi Data Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form action="/admin/siswa/update_data_siswa" method="POST">
                {{ csrf_field() }}
                @foreach ($siswa as $data)
                <div class="row">
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group" hidden>
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="..." value="{{ $data->id }}" name="xid" required>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" placeholder="..." value="{{ $data->siswa_nama }}" name="xnama" required>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>NISN</label>
                      <input type="text" class="form-control" placeholder="..." value="{{ $data->siswa_nisn }}" name="xnisn" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <!-- textarea -->
                    <div class="form-group">
                      <label>Alamat Siswa</label>
                      <textarea class="form-control" rows="3" placeholder="..." name="xalamat" required>{{ $data->siswa_alamat }}</textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Jenis Kelamin</label>
                        <?php if($data->siswa_jenis_kelamin == "p"){?>
                            <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio1" name="xjk" value="1">
                                <label for="customRadio1" class="custom-control-label">Laki-Laki</label>
                              </div>
                              <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio2" name="xjk" value="2" checked>
                                <label for="customRadio2" class="custom-control-label">Perempuan</label>
                              </div>
                        <?php }else{?>
                            <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio1" name="xjk" value="1" checked>
                                <label for="customRadio1" class="custom-control-label">Laki-Laki</label>
                              </div>
                              <div class="custom-control custom-radio">
                                <input class="custom-control-input" type="radio" id="customRadio2" name="xjk" value="2">
                                <label for="customRadio2" class="custom-control-label">Perempuan</label>
                              </div>
                        <?php }?>

                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label>Tanggal Masuk</label>
                        <input type="date" class="form-control" placeholder="..." name="xtglmasuk" value="{{ $data->siswa_tgl_masuk }}" required>
                      </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" class="form-control" placeholder="..." name="xtgllahir" value="{{ $data->siswa_tgl_lahir }}" required>
                          </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                        <!-- select -->
                        <div class="form-group">
                          <label>Agama</label>
                          <select class="form-control" name="xagama">
                            <option value="{{ $data->siswa_agama }}">{{ $data->siswa_agama }}</option>
                            <option value="islam">Islam</option>
                            <option value="protestan">Kristen Protestan</option>
                            <option value="katolik">Katolik</option>
                            <option value="hindu">Hindu</option>
                            <option value="buddha">Buddha</option>
                          </select>
                        </div>
                      </div>
                    {{-- <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" class="form-control" placeholder="..." name="xtgllahir" required>
                          </div>
                    </div> --}}
                  </div>
                  @endforeach
                  <div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                  </div>
                </div>
            </form>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

@endsection
