<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateJkOrtu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_orang_tua', function(Blueprint $t){
            $t->dropColumn('ortu_jk');
        });
        Schema::table('tb_orang_tua', function(Blueprint $t){
            $t->enum('ortu_jk', ['l', 'p'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
