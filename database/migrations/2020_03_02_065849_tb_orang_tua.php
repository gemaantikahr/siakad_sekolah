<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TbOrangTua extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_orang_tua', function(Blueprint $t){
            $t->bigIncrements('id');
            $t->string('ortu_nama');
            $t->string('ortu_nik');
            $t->string('ortu_alamat');
            $t->string('ortu_pekerjaan');
            $t->enum('ortu_jk', ['l', 'p']);
            $t->string('ortu_agama');
            $t->string('ortu_no_hp');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
