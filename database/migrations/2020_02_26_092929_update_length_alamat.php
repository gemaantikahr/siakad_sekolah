<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLengthAlamat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_siswa', function(Blueprint $table)
        {
            $table->dropColumn('siswa_alamat');
        });
        Schema::table('tb_siswa', function(Blueprint $table)
        {
            $table->string('siswa_alamat', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('tb_siswa', function (Blueprint $table) {
        //     $table->string('siswa_alamat', 500)->change();
        // });
    }
}
