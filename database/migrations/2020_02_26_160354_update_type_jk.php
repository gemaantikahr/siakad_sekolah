<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTypeJk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_siswa', function(Blueprint $t){
            $t->dropColumn('siswa_jenis_kelamin');
        });
        Schema::table('tb_siswa', function(Blueprint $t){
            $t->enum('siswa_jenis_kelamin', ['l','p'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
