<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TbSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_siswa', function(Blueprint $t){
            $t->bigIncrements('id');
            $t->string('siswa_nisn')->nullable();
            $t->string('siswa_nama')->nullable();
            $t->string('siswa_alamat')->nullable();
            $t->string('siswa_tgl_lahir')->nullable();
            $t->enum('siswa_jenis_kelamin', ['l', 'p']);
            $t->integer('siswa_agama')->nullable();
            $t->string('siswa__id_orang_tua')->nullable();
            $t->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
