<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsTglIdOrtu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_siswa', function (Blueprint $table) {
            $table->dropColumn(['siswa_tgl_lahir', 'siswa__id_orang_tua']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_siswa', function (Blueprint $table) {
            $table->dropColumn(['siswa_tgl_lahir', 'siswa__id_orang_tua']);
        });
    }
}
