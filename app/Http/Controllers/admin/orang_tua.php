<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psy\CodeCleaner\FunctionReturnInWriteContextPass;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class orang_tua extends Controller
{
    public function __construct()
    {
        if(!Session::get('login')){
            Redirect::to('admin/login')->send();
        }
    }
    public function index(){
        $data = array(
            'orang_tua' => DB::table('tb_orang_tua')->get(),
        );
        return view('admin/orang_tua/v_orang_tua', $data);
    }

    public function tambah_data(){
        return view('admin/orang_tua/v_tambah_orang_tua');
    }

    public function simpan_orang_tua(Request $req){
        if(isset($req->xpekerjaanlain)){
            $data = array(
                'ortu_nama' => $req->xnama,
                'ortu_nik' => $req->xnik,
                'ortu_alamat' => $req->xalamat,
                'ortu_jk' => $req->xjk,
                'ortu_no_hp' => $req->xnohp,
                'ortu_agama' => $req->xagama,
                'ortu_tgl_lahir' => $req->xnama,
                'ortu_pekerjaan' => $req->xpekerjaanlain,
                'created_at' => date('Y-m-d'),
            );
        }else{
            $data = array(
                'ortu_nama' => $req->xnama,
                'ortu_nik' => $req->xnik,
                'ortu_alamat' => $req->xalamat,
                'ortu_jk' => $req->xjk,
                'ortu_no_hp' => $req->xnohp,
                'ortu_agama' => $req->xagama,
                'ortu_tgl_lahir' => $req->xtgllahir,
                'ortu_pekerjaan' => $req->xpekerjaan,
                'created_at' => date('Y-m-d'),
            );
        }
        DB::table('tb_orang_tua')->insert($data);
        return redirect('admin/orang_tua');
    }
    public function hapus_ortu($id){
        DB::table('tb_orang_tua')->where('id', '=', $id)->delete();
        return redirect('admin/orang_tua');
    }

    public function edit_ortu($id){
        $data = array(
            'orang_tua' => DB::table('tb_orang_tua')->where('id', '=', $id)->get(),
        );
        return view('admin/orang_tua/v_edit_orang_tua', $data);
    }
    public function update_data_ortu(Request $req){
        $id = $req->xid;
        if(isset($req->xpekerjaanlain)){
            $data = array(
                'ortu_nama' => $req->xnama,
                'ortu_nik' => $req->xnik,
                'ortu_alamat' => $req->xalamat,
                'ortu_jk' => $req->xjk,
                'ortu_no_hp' => $req->xnohp,
                'ortu_agama' => $req->xagama,
                'ortu_tgl_lahir' => $req->xtgllahir,
                'ortu_pekerjaan' => $req->xpekerjaanlain,
                'updated_at' => date('Y-m-d'),
            );
        }else{
            $data = array(
                'ortu_nama' => $req->xnama,
                'ortu_nik' => $req->xnik,
                'ortu_alamat' => $req->xalamat,
                'ortu_jk' => $req->xjk,
                'ortu_no_hp' => $req->xnohp,
                'ortu_agama' => $req->xagama,
                'ortu_tgl_lahir' => $req->xtgllahir,
                'ortu_pekerjaan' => $req->xpekerjaan,
                'updated_at' => date('Y-m-d'),
            );
        }
        DB::table('tb_orang_tua')
            ->where('id', $id)
            ->update($data);
            return redirect('admin/orang_tua');
    }
}
