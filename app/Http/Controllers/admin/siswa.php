<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psy\CodeCleaner\FunctionReturnInWriteContextPass;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class siswa extends Controller
{
    public function __construct()
    {
        if(!Session::get('login')){
            Redirect::to('admin/login')->send()->with('alert', 'You must be loggin before');
        }
    }
    public function index(){
        $data = array(
            'siswa'=> DB::table('tb_siswa')->orderBy('id', 'desc')->get()
        );
        return view('admin/siswa/v_siswa', $data);
    }

    public function tambah_data(){
        return view('admin/siswa/v_tambah_siswa');
    }

    public function simpan_siswa(Request $req){
        $data = array(
            'siswa_nisn' => $req->xnisn,
            'siswa_nama' => $req->xnama,
            'siswa_alamat' => $req->xalamat,
            'siswa_jenis_kelamin' => $req->xjk,
            'siswa_agama' => $req->xagama,
            'siswa_tgl_lahir' => $req->xtgllahir,
            'siswa_tgl_masuk' => $req->xtglmasuk,
            'created_at' => date('Y-m-d')
        );
        DB::table('tb_siswa')->insert($data);
        return redirect('admin/siswa/');
    }
    public function edit_siswa($id){
        $data = array(
            'siswa' => DB::table('tb_siswa')->where('id', '=', $id)->get(),
        );
        return view('admin/siswa/v_edit_siswa', $data);
    }

    public function update_data_siswa(Request $req){
        $id = $req->xid;
        $data = array(
            'siswa_nisn' => $req->xnisn,
            'siswa_nama' => $req->xnama,
            'siswa_alamat' => $req->xalamat,
            'siswa_jenis_kelamin' => $req->xjk,
            'siswa_agama' => $req->xagama,
            'siswa_tgl_lahir' => $req->xtgllahir,
            'siswa_tgl_masuk' => $req->xtglmasuk,
            'updated_at' => date('Y-m-d')
        );
        DB::table('tb_siswa')->where('id', $id)->update($data);
        return redirect('admin/siswa/');
    }

    public function hapus_siswa($id){
        DB::table('tb_siswa')->where('id', '=', $id)->delete();
        return redirect('admin/siswa/');
    }
}
