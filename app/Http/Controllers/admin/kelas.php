<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class kelas extends Controller
{
    public function __construct()
    {
        if(!Session::get('login')){
            Redirect::to('admin/login')->send();
        }
    }
    public function index(){
        $kelas_aktif = DB::table('tb_kelas_aktif')->groupBy('id_kelas')->get();
        $banyak_siswa = array();

        foreach($kelas_aktif as $key){
            $banyak_kelas_siswa = DB::table('tb_kelas_aktif')->where('id_kelas', $key->id_kelas)->count();
            $banyak_siswa[$key->id_kelas] = $banyak_kelas_siswa;
        }
        $data = array(
            'kelas' => DB::table('tb_kelas')->join('tb_guru', 'tb_kelas.id_guru', '=', 'tb_guru.id')->select('tb_kelas.*', 'tb_guru.guru_nama')->get(),
            'guru' => DB::table('tb_guru')->get(),
            'banyak_siswa' => $banyak_siswa
        );

        return view('admin/kelas/v_kelas', $data);
    }
    public function tambah_kelas(Request $req){
        $data = array(
            'kelas_kelas' => $req->xkelas,
            'kelas_nama' => $req->xextensi,
            'id_guru' => $req->xguru,
            'created_at' => date('Y-m-d')
        );
        DB::table('tb_kelas')->insert($data);
        return redirect('admin/kelas');
    }

    public function edit_kelas(Request $req){
        $id = $req->xid;
        $data = array(
            'kelas_kelas' => $req->xkelas,
            'kelas_nama' => $req->xextensi,
            'id_guru' => $req->xguru,
            'updated_at' => date('Y-m-d')
        );
        DB::table('tb_kelas')->where('id', $id)->update($data);
        return redirect('admin/kelas');
    }
    public function hapus_kelas($id){
        DB::table('tb_kelas')->where('id', $id)->delete();
        DB::table('tb_kelas_aktif')->where('id_kelas', $id)->delete();
        return redirect('admin/kelas');
    }

    public function detail_kelas($id){
        $query = DB::table('tb_kelas')->where('id', $id)->first();
        $data = array(
            'kelas_nama' => $query->kelas_nama,
            'kelas_kelas' => $query->kelas_kelas,
            'guru' =>DB::table('tb_guru')->join('tb_kelas', 'tb_guru.id', '=', 'tb_kelas.id_guru')->where('tb_kelas.id', '=', $id)->get(),
            'siswa' =>DB::table('tb_kelas_aktif')->join('tb_kelas', 'tb_kelas_aktif.id_kelas', '=', 'tb_kelas.id')->join('tb_siswa', 'tb_kelas_aktif.id_siswa', '=', 'tb_siswa.id')->where('tb_kelas.id', '=', $id)->get(),
            'id_kelas' => $id,
            'kelas' => DB::table('tb_kelas')->get(),
            'jumlah_siswa' => DB::table('tb_kelas_aktif')->where('id_kelas', $id)->count(),
            'jumlah_siswa_l' => DB::table('tb_kelas_aktif')->join('tb_siswa', 'tb_kelas_aktif.id_siswa', '=', 'tb_siswa.id')->where('tb_siswa.siswa_jenis_kelamin', '=', 'l')->where('tb_kelas_aktif.id_kelas', '=', $id)->count(),
            'jumlah_siswa_p' => DB::table('tb_kelas_aktif')->join('tb_siswa', 'tb_kelas_aktif.id_siswa', '=', 'tb_siswa.id')->where('tb_siswa.siswa_jenis_kelamin', '=', 'p')->where('tb_kelas_aktif.id_kelas', '=', $id)->count(),
        );
        return view('admin/kelas/v_detail_kelas', $data);
    }

    public function load_data_mahasiswa(Request $request){
        if ($request->has('q')) {
            $cari = $request->q;
            $data = DB::table('tb_siswa')->select('id', 'siswa_nama')->where('siswa_nama', 'LIKE', "%$cari%")->orWhere('siswa_nisn', 'LIKE', "%$cari%")->get();
            return response()->json($data);
        }
    }

    public function tambah_siswa_kelas(Request $req){
        $idkelas = $req->idkelas;
        $data = array(
            'id_kelas' => $req->xidkelas,
            'id_siswa' => $req->xsiswa
        );
        DB::table('tb_kelas_aktif')->insert($data);
        // dd($data);
        // dd(route('ganteng', [$idkelas]));
        // return redirect(route('ganteng', 14));
        return redirect()->route('kelas.detail', [14]);
    }

    public function hapus_siswa_kelas($id){

    }
}
