<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\M_admin;

class login extends Controller
{
    public function index(){
        return view('admin/login/v_login_admin');
    }

    public function login_cek(Request $req){
        $pass = $req->password;
        $data = M_admin::where('admin_username', $pass)->first();
        if($data){
            if(Hash::check($pass, $data->admin_password)){
                Session::put('username', $data->admin_username);
                Session::put('login', TRUE);
                Session::put('level', $data->admin_level);
                return redirect('admin/guru');
            }
        }else{
            return redirect('admin/login')->with('alert', 'Password or username incorrect');
        }
    }

    public function register(){
        return view('admin/login/v_register');
    }
    public function save_akun(Request $req){
        $this->validate($req, [
            'admin_name' => 'required',
            'admin_username' => 'required|min:4|unique:tb_admin',
            'admin_password' => 'required',
            'admin_password1' => 'required|same:admin_password',
        ]);

        $data = new M_admin();
        $data->admin_name = $req->admin_name;
        $data->admin_password = bcrypt($req->admin_password);
        $data->admin_username = $req->admin_username;
        $data->created_at = date('Y-m-d');
        $data->save();
        return redirect('admin/login')->with('alert-success','Yeah You got it');
    }

    public function log_out(){
        Session::flush();
        return redirect('admin/login')->with('alert','Logout Success');
    }
}
