<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class guru extends Controller
{
    public function __construct()
    {
        if(!Session::get('login')){
            Redirect::to('admin/login')->send()->with('alert', 'You must be loggin before');
        }
    }
    public function index(){
        $data = array(
            'guru' => DB::table('tb_guru')->orderBy('id', 'desc')->get(),
        );
        return view('admin/guru/v_guru', $data);
    }

    // tambah guru
    public function tambah_guru(Request $req){
        if($req->has('xgambar')){
            $gambar = $req->file('xgambar');
            // $gambar->getClientOriginalName();
            $namagambar = sha1(date("h:i:sa").date("Y-m-d")).".".$gambar->getClientOriginalExtension();
            $gambar->getClientOriginalExtension();
            $gambar->getRealPath();
            $gambar->getSize();
            $gambar->getMimeType();
            $tujuan_upload='assets/images/guru';
            $gambar->move($tujuan_upload,$namagambar);

            $data = array(
                'guru_nama'=> $req->xnama,
                'guru_alamat'=>$req->xalamat,
                'guru_no_hp'=>$req->xnohp,
                'guru_photo'=>$namagambar,
                'guru_tgl_masuk'=> $req->xtglmasuk,
                'created_at'=> date('Y-m-d')
            );
        }else{
            $data = array(
                'guru_nama'=> $req->xnama,
                'guru_alamat'=>$req->xalamat,
                'guru_no_hp'=>$req->xnohp,
                'guru_photo'=>'noimage.png',
                'guru_tgl_masuk'=> $req->xtglmasuk,
                'created_at'=> date('Y-m-d')
            );
        }
        DB::table('tb_guru')->insert($data);
        return redirect('admin/guru');
    }

    //update teachher
    public function edit_guru(Request $req){
        $id = $req->xid;
        if($req->has('xgambar')){
            $user = DB::table('tb_guru')->where('id', $id)->first();
            $judul_gambar = $user->guru_photo;
            if($judul_gambar != "noimage.png"){
                $filename = public_path().'/assets/images/guru/'.$judul_gambar;
                \File::delete($filename);
            }
            $gambar = $req->file('xgambar');
            // $gambar->getClientOriginalName();
            $namagambar = sha1(date("h:i:sa").date("Y-m-d")).".".$gambar->getClientOriginalExtension();
            $gambar->getClientOriginalExtension();
            $gambar->getRealPath();
            $gambar->getSize();
            $gambar->getMimeType();
            $tujuan_upload='assets/images/guru';
            $gambar->move($tujuan_upload,$namagambar);

            $data = array(
                'guru_nama' => $req->xnama,
                'guru_alamat' => $req->xalamat,
                'guru_no_hp' => $req->xnohp,
                'guru_tgl_masuk' => $req->xtglmasuk,
                'updated_at' => date('Y-m-d'),
                'guru_photo'=>$namagambar
            );

        }else{
            $data = array(
                'guru_nama'=> $req->xnama,
                'guru_alamat'=>$req->xalamat,
                'guru_no_hp'=>$req->xnohp,
                'guru_tgl_masuk'=> $req->xtglmasuk,
                'updated_at' => date('Y-m-d')
            );
        }
        DB::table('tb_guru')->where('id', $id)->update($data);
        return redirect('admin/guru');

    }

    public function hapus_guru($id){
        $user = DB::table('tb_guru')->where('id', $id)->first();
        $judul_gambar = $user->guru_photo;
        if($judul_gambar != "noimage.png"){
            $filename = public_path().'/assets/images/guru/'.$judul_gambar;
            \File::delete($filename);
        }
        DB::table('tb_guru')->where('id', '=', $id)->delete();
        return redirect('admin/guru');
    }
}
