<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});


// admin
Route::get('admin/login', 'admin\login@index');
Route::POST('admin/login/login_cek', 'admin\login@login_cek');
Route::get('admin/login/register', 'admin\login@register');
Route::get('admin/login/log_out', 'admin\login@log_out');
Route::POST('admin/login/save_akun', 'admin\login@save_akun');


// guru
Route::get('admin/guru', 'admin\guru@index');
Route::post('admin/guru/tambah_guru', 'admin\guru@tambah_guru');
Route::post('admin/guru/edit_guru', 'admin\guru@edit_guru');
Route::get('admin/guru/hapus_guru/{id}', 'admin\guru@hapus_guru');
// guru

//siswa
Route::get('admin/siswa', 'admin\siswa@index');
Route::get('admin/siswa/tambah_data', 'admin\siswa@tambah_data');
Route::post('admin/siswa/simpan_siswa', 'admin\siswa@simpan_siswa');
Route::get('admin/siswa/edit_siswa/{id}', 'admin\siswa@edit_siswa');
Route::post('admin/siswa/update_data_siswa', 'admin\siswa@update_data_siswa');
Route::get('admin/siswa/hapus_siswa/{id}', 'admin\siswa@hapus_siswa');
//siswa

//kelas
Route::get('admin/kelas', 'admin\kelas@index');
Route::post('admin/kelas/tambah_kelas', 'admin\kelas@tambah_kelas');
Route::post('admin/kelas/edit_kelas','admin\kelas@edit_kelas');
Route::post('admin/kelas/tambah_siswa_kelas','admin\kelas@tambah_siswa_kelas');
Route::get('admin/kelas/hapus_kelas/{id}', 'admin\kelas@hapus_kelas');
Route::get('admin/kelas/detail_kelas/{id}', ['as' => 'kelas.detail', 'uses' => 'admin\kelas@detail_kelas']);
Route::get('admin/kelas/hapus_siswa_kelas/{id}', 'admin\kelas@hapus_siswa_kelas');
Route::get('/cari', 'admin\kelas@load_data_mahasiswa');
//kelas

// orang tua
Route::get('admin/orang_tua', 'admin\orang_tua@index');
Route::get('admin/orang_tua/tambah_data', 'admin\orang_tua@tambah_data');
Route::post('admin/orang_tua/simpan_orang_tua', 'admin\orang_tua@simpan_orang_tua');
Route::get('admin/orang_tua/hapus_ortu/{id}', 'admin\orang_tua@hapus_ortu');
Route::get('admin/orang_tua/edit_ortu/{id}', 'admin\orang_tua@edit_ortu');
Route::post('admin/orang_tua/update_data_ortu', 'admin\orang_tua@update_data_ortu');
// orang tua



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
